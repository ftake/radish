Rails.application.routes.draw do
  get 'view/registration'
  devise_for :users
  resources :events
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'events#index'

  resources :events do
    resources :registrations
  end
end
