class Event < ApplicationRecord
  has_many :registrations
  has_many(
    :organizers, -> { where('registrations.is_organizer = true') },
    through: :registrations, source: :user
  )
  has_many(
    :attendees, -> { where('registrations.canceled = false') },
    through: :registrations, source: :user
  )
  has_many(
    :cancels, -> { where('registrations.canceled = true') },
    through: :registrations, source: :user
  )
end
