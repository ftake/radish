class EventsController < ApplicationController
  include AuthenticationHelper

  before_action :set_event, only: %i[show edit update destroy]
  before_action :authenticate_user, only: %i[new edit update destroy create]
  before_action :authenticate_organizer!, only: %i[edit update destroy]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    ActiveRecord::Base.transaction do
      @event.save!

      # 作成者を organizer にする
      organizer = Registration.new(
        event_id: @event.id,
        user_id: current_user.id,
        is_organizer: true
      )
      organizer.save!
    end

    respond_to do |format|
      format.html { redirect_to @event, notice: 'Event was successfully created.' }
      format.json { render :show, status: :created, location: @event }
    end
# TODO
#  rescue => e
#    respond_to do |format|
#      format.html { render :new }
#      format.json { render json: @event.errors, status: :unprocessable_entity }
#    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:title, :start, :end, :location, :main_content)
  end
end
