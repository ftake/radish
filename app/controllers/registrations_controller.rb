class RegistrationsController < ApplicationController
  include AuthenticationHelper

  before_action :set_event, only: %i[new create]
  before_action :authenticate_user, only: %i[new create]

  def new
    @registration = Registration.new
  end

  def create
    @registration = @event.registrations.create(registration_params)
    # non-organizer cannot set is_organizer flag
    authenticate_organizer! if @registration.is_organizer
    @registration.save!

    respond_to do |format|
      format.html {
        redirect_to(
          @event,
          notice: 'Finished registration to this event successfully.'
        )
      }
      format.json {
        render :show, status: :created, location: @registration
      }
    end

    # TODO: error handling
  end

  def registration_params
    params.permit(:canceled, :is_organizer).merge(user_id: current_user.id)
  end

  def set_event
    @event = Event.find(params[:event_id])
  end
end
