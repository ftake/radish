module EventsHelper
  def render_md(md_text)
    renderer = CustomRender.new(render_options = {})
    markdown = Redcarpet::Markdown.new(renderer, autolink: true)

    raw(markdown.render(md_text))
  end

  class CustomRender < Redcarpet::Render::XHTML
    def header(text, level)
      level += 1 
      %(<h#{level} class="ui header">#{text}</h#{level}>)
    end
  end

  DAYS_OF_WEEK = %w[日 月 火 水 木 金 土]
  def format_date(datetime)
    # TODO: ユーザ設定から読み込み
    #d = datetime.in_time_zone('Asia/Tokyo')
    d = datetime
    wday = DAYS_OF_WEEK[d.wday]
    d.strftime("%Y/%m/%d (#{wday}) %H:%M %Z")
  end

  def organizers(event)
    r = event.attendees.where('attendees.is_organizer = true')
    r.map do |a|
      a.user
    end
  end
end
