# Provides authentication functions for controller classes
module AuthenticationHelper
  def authenticate_user
    unless user_signed_in?
      session[:previous_url] = request.fullpath
      redirect_to new_user_session_path, notice: 'サインインが必要です'
    end
  end

  def authenticate_organizer!
    raise NotOrganizer('このイベントの編集権がありません') unless
      @event.organizers.include?(current_user)
  end
end
